/**
 * Created by maxiaoling on 2017/6/26.
 */
var express      = require ('express');
var path         = require ('path');
var cookieParser = require ('cookie-parser');
var bodyParser = require('body-parser');
var fs = require('fs');

// init express instance
var app = new express();

require('./proxy')(app,"47.94.129.86");

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
//
// if (process.env.NODE_ENV !== 'production' || env !== 'production') {
//   require('./app')(app,cwd,env)
// }

// default port 3000
var port = process.env.PORT || 8000;

app.use('/', express.static(path.resolve('dist')))

app.get('/', function(req, res) {
  var f = './dist/index.html';
  res.sendFile(f);
});

app.get('/orders', function(req, res) {
  var f = path.resolve(__dirname, '../dist/orders.html');
  res.sendFile(f);
});


app.listen(port, function(error) {
  if (error) {
    console.error(error);
  } else {
    console.info("==> 🌎  Listening on port %s. Open up http://localhost:%s/ in your browser.", port, port);
  }
});


app.use(cookieParser());

