const path = require('path');
const pxtorem = require('postcss-pxtorem');

const svgSpriteDirs = [
	require.resolve('antd-mobile').replace(/warn\.js$/, ''), // antd-mobile 内置svg
	path.resolve(__dirname, 'src/assets/svg/'),  // 业务代码本地私有 svg 存放目录
];

export default {
	entry: 'src/index.js',
	svgSpriteLoaderDirs: svgSpriteDirs,
	proxy: {
		"/seckill-login": {
			"target": "http://47.94.129.86:8000",
			"changeOrigin": true
		},
    "/seckill": {
      "target": "http://47.94.129.86:8000",
      "changeOrigin": true
    }
	},
	env: {
		development: {
			extraBabelPlugins: [
				'dva-hmr',
				'transform-runtime',
				['import', { 'libraryName': 'antd-mobile', 'libraryDirectory': 'lib', 'style': true }]
			],
			extraPostCSSPlugins: [
				pxtorem({
					rootValue: 100,
					propWhiteList: [],
				}),
			],
		},
		production: {
			extraBabelPlugins: [
				'transform-runtime',
				['import', { 'libraryName': 'antd-mobile', 'libraryDirectory': 'lib', 'style': true }]
			],
			extraPostCSSPlugins: [
				pxtorem({
					rootValue: 100,
					propWhiteList: [],
				}),
			],
		}
	}
}
