# 秒杀app



![页面效果图](./imageForReadme/seckill-mobile-index.png)


## 用法

开发模式

```
npm i ;
npm start;
```

编译：

```
npm run build
```
pm2启动：

```
npm test
```

## 代理配置

代理信息的配置在根目录的`.roadhogrc.js`的`proxy`属性，例子如下：


```

proxy: {
  "/seckill": {
    "target": "http://112.74.93.133:8000",
    "changeOrigin": true
    }
}

```
更多的配置可以参考[http-proxy-middleware](https://github.com/chimurai/http-proxy-middleware)，内部实现是由[http-proxy-middleware](https://github.com/chimurai/http-proxy-middleware)实现的

## 技术堆栈

- [mobile ant design](https://mobile.ant.design/index-cn) :  移动端的UI组件
- [dva](https://github.com/dvajs/dva) : model层
- [roadhog](https://github.com/sorrycc/roadhog) : 用于编译
