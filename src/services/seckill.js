import request from '../utils/request';
import {getUserId} from '../utils/token'


export async function buy(url) {
  return request(url, {
    method: 'POST',
    body: JSON.stringify({
      userId: getUserId()
    })
  })
}

export async function products(){
  return request(`/seckill-login/products`);
}


export async function info(productId) {
  return request(`/seckill/accesspoint/api/v1/products/${productId}`)
}

export async function status(productId,requestId) {
  return request(`/seckill/accesspoint/api/v1/status/${productId}?requestId=${requestId}`)
}

export async function pay(orderId,productId) {
  let url = `/seckill/orderhandle/api/v1/orders/pay/${orderId}`;
  return request(url,{
    method: 'PUT',
    data: JSON.stringify({
      productId: productId
    })
  })
}
