import request from '../utils/request';

export async function list() {
	return request('/seckill/orderhandle/api/v1/D2F75146B0C215E3C510B396FD920400s889')
}
export async function myOrdersList(userId,productId) {
  console.log(userId)
  return request(`/seckill/orderhandle/api/v1/users/${userId}/products/${productId}?type=orderHandle`)
}
