import moment from 'moment'
import {list, myOrdersList} from '../services/orders'
import {routerRedux} from 'dva/router';
import {getUserId} from "../utils/token";

export default {

  namespace: 'orders',

  state: {
    items: [],
    orderList:[]
  },

  subscriptions: {
    setup({dispatch, history}) {
      return history.listen(({pathname, query}) => {
        if (pathname === '/myOrders') {
          // dispatch({type: 'list', payload: query});
        }
      });
    },
  },

  effects: {
    * list(payload, {call, put}) {  // eslint-disable-line
      // const {data} = yield call(list)
      // yield put({
      //   type: 'update',
      //   payload: {
      //     items: data.payload
      //   }
      // });
    },
    * myOrders(payload, {call, put}) {
      // console.log(payload)
      const {data} = yield call(myOrdersList.bind(null,payload.userId,payload.id))
      // console.log(data);
      if(data) {
        yield put({
          type: 'update',
          payload: {
            orderList: data.payload
          }
        });
      }
    }
  },

  reducers: {
    update(state, action) {
      return {...state, ...action.payload};
    }
  }

};
