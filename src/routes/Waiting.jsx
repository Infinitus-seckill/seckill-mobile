import React from 'react';
import {connect} from 'dva';
import moment from 'moment';
import {Button, Modal, NavBar, Icon, List, Card, Flex, ActivityIndicator, Result} from 'antd-mobile';
import { routerRedux } from 'dva/router';
const Item = List.Item;
const Brief = Item.Brief;

let inter

class OrderMessage extends React.Component {
	constructor(props) {
		super(props)

		this.state = {
			queuing:{
				type:'check-circle',
				color:'green',
				content:'抢购提交成功',
				brief:'正在排队',
			},
			end:{
				type:'cross-circle-o',
				color:'gray',
				content:'秒杀已经结束',
				brief:'秒杀失败',
			}
		}

	}
  componentWillUnmount() {
    clearInterval(inter)
  }
  componentDidMount() {
    window.onbeforeunload = () => 'tips'
    let id = this.props.location.query.id,
      requestId = encodeURIComponent(this.props.location.query.requestId) ;
    inter = setInterval(() => this.props.dispatch({type: 'seckill/status', timer: inter, id: id, requestId: requestId}), 2000)
  }

	render() {

		const {queuing = false, seckillStatus = 'end', shouldWaiting} = this.props ;
		const result = this.state[queuing?'queuing':'end']
		const brief = queuing?`${result.brief},预计${moment().add(5,'m').format('HH:mm')}出结果`:result.brief


		return (
			<div className="waiting">
        <NavBar mode="light" iconName={null}>
          秒杀状态页
        </NavBar>
        <ActivityIndicator
          toast
          text="排队中..."
          animating={true}
        />
        {

          // <NavBar leftContent="返回" mode="light" onLeftClick={this.handleContinueShopping}>
          //   秒杀POC
          // </NavBar>
          // <Card full>
          //   <Card.Body>
          //     <List className="my-list">
          //       <Item extra={moment().format('HH:mm:ss')}
          //             align="top"
          //             thumb={<Icon type={result.type} color={result.color} />} multipleLine>
          //         {result.content}
          //         <Brief>{brief}</Brief>
          //       </Item>
          //     </List>
          //   </Card.Body>
          //   <Card.Footer
          //     content={<Button type="ghost" inline onClick={this.handleShowOrder}>查看订单</Button>}
          //     extra={<Button type="ghost" inline onClick={this.handleContinueShopping}>继续购物</Button>}
          //   />
          // </Card>
        }
			</div>
		);
	}


	handleShowOrder = event => {
		this.props.dispatch(routerRedux.push('/myOrders'))
	}

	handleContinueShopping = event => {
		this.props.dispatch({
			type: 'seckill/continueShopping'
		})
	}

}


export default connect(state=> {
	return state.seckill.state || {}
})(OrderMessage);
