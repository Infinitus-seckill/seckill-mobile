import React from 'react';
import {connect} from 'dva';
import styles from './IndexPage.css';
import {Button, Modal, NavBar, Icon, ActivityIndicator} from 'antd-mobile';
import Login from '../components/Login'
import {setProductId} from '../utils/token'

const alert = Modal.alert;
import SeckillCarousel from '../components/seckillCarousel'
import ProductDetail from '../components/productDetail'
import {routerRedux} from 'dva/router';

/**
 * 抢购页面
 */
class IndexPage extends React.Component {
  constructor(props) {
    super(props)
  }

  callback = () => {
    debugger
    this.props.dispatch({type: 'seckill/productInfo', payload: this.props.location.query})
  }

  render() {
    let {seckillInfo} = this.props
    let disabled
    if (!seckillInfo) {
      disabled = true
    } else {
      disabled = seckillInfo.startTime > seckillInfo.currentTime || seckillInfo.currentTime > seckillInfo.endTime || seckillInfo.stockCount <= 0
    }

    return (
      <div className={styles.container}>
        <ActivityIndicator
          toast
          text="加载中..."
          animating={!!this.props.loading}
        />
        <NavBar mode="light"
                iconName={null}
                rightContent={[
                  <Icon key="1" type="ellipsis" onClick={this.handleMyOrder}/>,
                ]}>
          商品详情
        </NavBar>
        <Login cb={this.callback}/>
        <SeckillCarousel
          disabled={disabled}
          product={this.props.product}
          seckillInfo={this.props.seckillInfo}
          onExpired={this.handleAllowSeckill}/>
        <ProductDetail detail={this.props.seckillInfo}/>
        <div className={styles.buttonWrap}>
          {
            disabled ? <Button disabled style={{
                position: 'fixed',
                bottom: 0,
                right: 0,
                left: 0,
                borderRadius: 0
              }}>立即抢购</Button> :
              <Button className={styles.goBuyBtn}
                      onClick={this.handleBuy}
                      type="primary">立即抢购</Button>
          }
        </div>
      </div>
    );
  }

  handleAllowSeckill = () => {
    this.props.dispatch({
      type: 'seckill/allowSeckill',
      payload: {
        startTime: this.props.seckillInfo.startTime,
        currentTime: this.props.seckillInfo.startTime + 1
      }
    })
  }

  handleMyOrder = () => {
    setProductId(this.props.location.query.id);
    // window.location.href="/myOrders";
    // this.props.history.push(`/myOrders/${id}`)
    this.props.dispatch({
      type: 'seckill/myOrders',
      id: this.props.location.query.id
    })
  }
  handleBuy = () => {
console.log(this)
    setProductId(this.props.location.query.id);
    this.props.dispatch({
      type: 'seckill/buy',
      id: this.props.location.query.id
    })
  }
}


export default connect(state => {
  return {
    loading: state.loading.models.seckill,
    ...state.seckill
  }
})(IndexPage);
