/**
 * Created by levy on 2017/6/1
 */
import React from 'react'
import {connect} from 'dva';
import { routerRedux } from 'dva/router';
import { List, Brief, NavBar, Card } from 'antd-mobile'
import Login from '../components/Login'
import style from './style.css'
import {deleteToken} from '../utils/token'

const Item = List.Item

// const id = 'B879FE39D347EEF50175E57859830289'
// const id = 'A879FE39D347EEF50175E57859830503'

class ProductList extends React.Component {
  componentDidMount() {

    this.props.dispatch({type: 'seckill/products', payload: {cb:(id)=> {
      if(id) this.props.dispatch({type: 'seckill/productInfo', payload: {id}})
    }}})
  }

  logout = () => {
    deleteToken()
    window.location.reload()
  }

  render() {
    const products = this.props.products || []
    return (
        <div className="product-list">
          <NavBar
            leftContent="登出"
            onLeftClick={this.logout}
            mode="light" iconName={null}>
            商品列表
          </NavBar>
          <Login cb={() => this.props.dispatch({type: 'seckill/productInfo', payload: {id:this.props.products[0].id || ""}})}/>
          <div className="list">
            {products.map((item,index)=>{
              return <div key={index} className={style.item}
                  onClick={this.goDetail.bind(this, item.id)}
                >
                <img className={style.itemImg}
                  src={this.props.seckillInfo.productImg || ''}/>
                <div className={style.text}>
                  <div className={style.title}>{this.props.seckillInfo.productName || ''}</div>
                  <div><span className={style.price}> {'¥' + (this.props.seckillInfo.seckillPrice || '')} </span></div>
                  <div style={{textAlign: 'right'}}>
                    <button className={style.goBuy}>去抢购</button>
                  </div>
                </div>
              </div>
            })}
          </div>
          {
            /*<Item thumb="https://zos.alipayobjects.com/rmsportal/XmwCzSeJiqpkuMB.png"
                  arrow="horizontal"
                  onClick={this.goDetail.bind(this, 'B879FE39D347EEF50175E57859830258')}
            >商品2-库存多</Item>
            <Item thumb="https://zos.alipayobjects.com/rmsportal/XmwCzSeJiqpkuMB.png"
                  arrow="horizontal"
                  onClick={this.goDetail.bind(this, 'B879FE39D347EEF50175E57859830255')}
            >商品3-库存少</Item>
            */
          }
        </div>
    )
  }
  goDetail (id) {
    // location.href = '/seckill?id=' + id
    this.props.dispatch(routerRedux.push({
      pathname: '/seckill',
      query: {
        id
      }
    }))
  }
}

export default connect(state=> {
  return state.seckill || {}
})(ProductList);
