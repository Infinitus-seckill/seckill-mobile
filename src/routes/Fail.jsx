/**
 * Created by levy on 2017/6/5
 */
import React from 'react'
import { Icon,  Result} from 'antd-mobile';
import {Button, WingBlank} from 'antd-mobile';


export default class fail extends React.Component {

  onBack = () => {
    history.back();
  }
  render() {
    const {message}  = this.props.location.query;
    return (
        <div className="fail">
          <Result
            className="result"
            img={<Icon type="cross-circle-o" className="icon" style={{ fill: '#F13642', width: '1.2rem', height: '1.2rem'}} />}
            title="秒杀失败"
            message={message}
          />
          <div style={{marginTop:80}}>
            <WingBlank>
              <Button type="primary" onClick={this.onBack}>返回上一级</Button>
            </WingBlank>
          </div>
        </div>
    )
  }
}
