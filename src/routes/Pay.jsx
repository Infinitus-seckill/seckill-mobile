/**
 * Created by levy on 2017/6/2
 */
import React from 'react'
import {Button, WingBlank, NavBar, WhiteSpace} from 'antd-mobile';
import {connect} from 'dva';
import {getUserId} from '../utils/token'

class Pay extends React.Component {


  onClick = () => {
    /*
    const userId = getUserId()
    const orderId = this.props.status && this.props.status.orderId ? this.props.status.orderId: ""
    alert(`用户id:${userId},订单号:${orderId}`);
    */
    this.props.dispatch({
      type: 'seckill/myOrders',
      id: this.props.location.query.id
    })
  }

  onBack = (ablePay) => {
    const {orderId, id} = this.props.location.query;
    if (ablePay && orderId) {

      this.props.dispatch({
        type: 'seckill/pay',
        id: id,
        orderId: orderId
      })
    }
    else {
      history.back();
    }
  }

  render() {
    const {ablePay, message} = this.props.location.query;
    return (
      <div className="pay">
        {/*
            this.props.status.isSuccess == "exist" &&
            <div>
              <NavBar mode="light" iconName={null}>
                支付页面
              </NavBar>
              <WhiteSpace/>
              <h3 style={{textAlign: 'center'}}>个人名额已购满</h3>
              <WingBlank>
                <Button type="primary" onClick={this.onClick}>继续支付已有名额</Button>
              </WingBlank>
            </div>
           */}

        <NavBar mode="light" iconName={null}>
          {ablePay === "true" ? '抢购结果页面' : '支付结果界面'}
        </NavBar>
        <WhiteSpace/>
        <h1 style={{textAlign: 'center'}}>{message}</h1>
        <div style={{marginTop: 80}}>
          <WingBlank>
            <Button type="primary" onClick={this.onClick}>查看我的订单</Button>
          </WingBlank>
        </div>
        <WhiteSpace/>
        <div>
          <WingBlank>
            <Button type="primary"
                    onClick={() => this.onBack(ablePay === "true")}>{ablePay === "true" ? '我要支付' : '返回上一级'}</Button>
          </WingBlank>
        </div>
      </div>
    )
  }
}

export default connect(state => {
  return state.seckill || {}
})(Pay);
