/**
 * Created by levy on 2017/6/1
 */
import React from 'react'
import style from './style.css'

export default class App extends React.Component {
  render() {
    return (
        <div className="app">{this.props.children}</div>
    )
  }
}

