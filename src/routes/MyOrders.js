import React from 'react';
import {connect} from 'dva';
import moment from 'moment';
import {Button, ActivityIndicator, NavBar, Icon, List, Card, Flex} from 'antd-mobile';
import {routerRedux} from 'dva/router';
import {getUserId} from "../utils/token";

const Item = List.Item;
const Brief = Item.Brief;


class OrderMessage extends React.Component {

  componentWillMount() {
    this.handleShowOrder();
  }

  render() {
    return (
      <div>
        <NavBar leftContent="返回"
                mode="light"
                onLeftClick={this.handleContinueShopping}
                rightContent={[
                  <Icon key='2' onClick={this.handleShowOrder} type={require('../assets/svg/reload.svg')}/>
                ]}
        >
          我的订单
        </NavBar>
        <ActivityIndicator
          toast
          text="获取列表中..."
          animating={!!this.props.loading}
        />

        <Card full>
          <Card.Body>
            {
              this.props.orderList.length?
                <List className="my-list">
                  {
                    this.props.orderList.slice(-this.props.orderList.length).reverse().map((item, index) => {
                      return (
                        <Item
                          key={index}
                          thumb={<img style={{width: 60, height: 60}}
                                      src={require(`../assets/${index % 3 + 1}.jpg`)}/>} multipleLine>
                          <div style={{padding:5, fontSize:14, color:"#a1a1a1"}}>订单编号：{item.requestId}</div>
                          <div style={{padding:5,fontSize:14, color:"#a1a1a1"}}>订单日期：{moment(item.seckillTime).format('YY/MM/DD HH:mm')}</div>
                        </Item>
                      )
                    })
                  }
                </List> :
                <div style={{margin:20, textAlign:"center", fontSize:20, color:"#bbbbbb"}}>暂无订单 </div>
            }
          </Card.Body>
        </Card>
      </div>
    );
  }


  handleShowOrder = event => {

    let userId = getUserId()
    this.props.dispatch({
      type: 'orders/myOrders',
      id: this.props.location.query.id,
      userId: userId
    })
  }

  handleContinueShopping = event => {
    // this.props.dispatch(routerRedux.push('/'))
    history.back()
  }

}


export default connect(state => {
  return {
    loading: state.loading.models.orders,
    ...state.orders
  }
})(OrderMessage);
