import React from 'react';
import { Router, Route, browserHistory, IndexRoute } from 'dva/router';
import Seckill from './routes/IndexPage';
import Waiting from './routes/Waiting';
import MyOrders from './routes/MyOrders';
import ProductList from './routes/ProductList';
import Pay from './routes/Pay'
import App from './routes/App';
import Fail from './routes/Fail';

function RouterConfig({history}) {
  return (
    <Router history={history}>
      <Route path="/" component={App}>
        <IndexRoute component={ProductList}/>
      </Route>
      <Route path="/productList" component={ProductList} />
      <Route path="/seckill" component={Seckill} />
      <Route path="/waiting" component={Waiting} />
      <Route path="/pay" component={Pay} />
      <Route path="/fail" component={Fail} />
      <Route path="/myOrders" component={MyOrders} />
    </Router>
  );
}

export default RouterConfig;
