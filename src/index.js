import dva from 'dva';
import './index.html';
import moment from 'moment'
import createLoading from 'dva-loading';

window.moment = moment
// 1. Initialize
const app = dva(createLoading());

// 2. Plugins
// app.use({});

// 3. Model
app.model(require('./models/seckill'));
app.model(require('./models/orders'));

// 4. Router
app.router(require('./router'));

// 5. Start
app.start('#root');
