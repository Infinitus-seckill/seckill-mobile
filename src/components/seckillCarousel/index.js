import React from 'react';
import styles from './index.css';
import {Carousel, Flex, Button, List} from 'antd-mobile';
import Countdown from '../Countdown'
import moment from 'moment'

export default function IndexPage({product, onExpired, seckillInfo, disabled}) {

  return (
    <div className={styles.container}>
      <div style={{marginTop: 5}}>
        <Carousel
          autoplay={false} infinite
          // beforeChange={(from, to) => console.log(`slide from ${from} to ${to}`)}
          // afterChange={index => console.log('slide to', index)}
        >
          {[1, 2, 3].map((ii, index) => (
            <img className={styles.img} src={seckillInfo.productImg || ""} key={index}/>
          ))}
        </Carousel>
      </div>
      <Flex style={{padding: '0 0 0 15px'}} className={styles.priceInfo}>
        <Flex.Item style={{flex: 1.5}}>
						<span
              className={styles.discountPrice}>{'¥' + (seckillInfo.seckillPrice || '')}</span>
        </Flex.Item>

        <Flex.Item style={{flex: 3}}>
          <Button className={styles.btn}
                  activeStyle={false}
                  type="primary">
            {
              seckillInfo.currentTime < seckillInfo.startTime ? <CountTime onExpired={onExpired}
                                                                           startDate={seckillInfo.currentTime}
                                                                           endDate={seckillInfo.startTime}
              /> : disabled ? '已结束' : `抢购中`
            }
          </Button>
          {
            // <div className={styles.buttonWrap}>
            // </div>
            // <div style={{width:"100%"}}>
            // ${moment(seckillInfo.startTime,'x').format("YYYY/MM/DD HH:mm:ss")}
            // </div>
          }
        </Flex.Item>
      </Flex>
    </div>
  );
}

function CountTime({startDate, endDate, onExpired}) {
  const OPTIONS = {
    endDate: endDate,
    startDate: startDate,
    prefix: '距离开始:',
    onExpired
  }

  return (
    <Flex>
      <Flex.Item style={{flex: 2}}>
        <Countdown options={OPTIONS} onExpired={onExpired}/>
      </Flex.Item>
    </Flex>
  )
}
