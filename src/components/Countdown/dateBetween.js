let DateBetween = function(startDate, endDate) {
	let second = 1000;
	let minute = second * 60;
	let hour = minute * 60;
	let day = hour * 24;
	let distance = endDate - startDate;

	if (distance < 0) {
		return false;
	}

	let days = Math.floor(distance / day);
	let hours = Math.floor((distance % day) / hour);
	let minutes = Math.floor((distance % hour) / minute);
	let seconds = Math.floor((distance % minute) / second);


	let result = {
		days,hours,minutes,seconds,
		hasDays:!!hours,
		isExpired: ! (days || hours || minutes || seconds)
	}
	return result
}

module.exports = DateBetween;