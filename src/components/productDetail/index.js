import React,{Component,PropTypes} from 'react';
import {List} from 'antd-mobile';
import Styles from './index.css';
const Item = List.Item;
const Brief = Item.Brief;

export default class ProductDetail extends Component {

  renderItem (){

    const {productParameter} = this.props.detail;
    if(!productParameter || productParameter.length < 1) return
    let productInfo =  JSON.parse(productParameter);
    let items = [];
    let i = 0;
    for(let key in productInfo) {
      i++;
      items.push(
        <tr className="product-attr-item" key={i}>
        <td>{key+":"}</td>
        <td>{productInfo[key]||''}</td>
        </tr>
      )
    }
    return items;
  }

  render() {
    const {productName,productDesc,productBrand} = this.props.detail

    return (
      <div className={Styles.productAttr}>
        <div className={Styles.title}>{productName}
        </div>
        <div className={Styles.title}>
          商品描述
          <div className={Styles.detail}>
            {productDesc}
          </div>
        </div>
        <div className={Styles.title} style={{border: 0}}>
          商品信息
          <table className={Styles.table}>
            <tbody>
            {this.renderItem()}
            </tbody>
          </table>
        </div>
      </div>
    )
  }


}

ProductDetail.propTypes = {
  detail: PropTypes.object.isRequired
}
