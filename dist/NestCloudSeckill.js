function NestCloudSeckill(){

}
$.extend(NestCloudSeckill.prototype, {


//初始化获取参数
    init: function(_httpHost, _productid, _userId, _token) {
        this.currentMicSecond=0;        //当前时间（毫秒）
        this.serverStartMicSecond=0;    //服务开始时间(毫秒)
        this.serverEndMicSecond=0;     //服务结束时间(毫秒)
        this.httpHost=_httpHost;   //前缀
        this.productid = _productid;   //商品编
        this.userId = _userId;         //用户ID
        this.token = _token;          //权限令牌
        this.timer=null;             //时间器
        this.seckillURL="";                //下发的接口
        this.updateTimeCallback=null; //时间更新的回调
    },
    
    //秒杀的状态。0为未进行加载，1为未到开始时间 ，2为活动进行中，3为活动已结束
    seckStatus:function(){
        if(this.currentMicSecond<1000 || this.serverStartMicSecond<1000 || this.serverEndMicSecond<1000 ){
            return 0;
        }
        if(this.currentMicSecond<this.serverStartMicSecond){
            return 1;
        }
        if(this.currentMicSecond>=this.serverStartMicSecond && this.currentMicSecond < this.serverEndMicSecond){
            return 2;
        }
        return 3;
    },

    seckStatusDate:function(startDate,endDate){

        var second = 1000;
        var minute = second * 60;
        var hour = minute * 60;
        var day = hour * 24;
        var distance = endDate - startDate;

        if (distance < 0) {
            return false;
        }

        var days = Math.floor(distance / day);
        var hours = Math.floor((distance % day) / hour);
        var minutes = Math.floor((distance % hour) / minute);
        var seconds = Math.floor((distance % minute) / second);

        return result = {
            days:days,
            hours:hours,
            minutes:minutes,
            seconds:seconds,
            hasDays:!!hours,
            isExpired: ! (days || hours || minutes || seconds)
        };
    },

    timeEventUpdate:function(){
        var self=this;
        if(self.timer!=null){
            clearTimeout(self.timer);
            self.timer=null;
        }
        if(self.currentMicSecond<self.serverEndMicSecond){
            self.timer=setTimeout(function(){
                self.currentMicSecond+=1000;
                if(self.updateTimeCallback!=null)
                    self.updateTimeCallback(self);
                self.timeEventUpdate();
            },1000);
        }
    },


    //商品秒杀
    //成功回调successCall(seckillStatus)  seckillStatus #秒杀状态目前有两种，queuing:排队中，end:秒杀结束
    //失败回调failedCall(json)
    seckill: function(successCall,failedCall) {

        return $.ajax({
            url: this.httpHost+ this.seckillURL+"?userId="+this.userId+"&token="+this.token,
            type: "POST",
            dataType:'json',
            success: function(data) {
                if(data!=null && data.seckillStatus!=null){
                    if(successCall!=null)
                        successCall(data.seckillStatus);
                }
                else if(failedCall!=null)
                    failedCall(data);
            },
            error: function(data) {
                if(failedCall!=null)
                    failedCall(data);
            }
        });
    },


    //获取下发url
    //成功回调successCall()    返回下发的url
    //失败回调failedCall(json)

    seckillapi: function(successCall,failedCall) {
        var self=this;
        return $.ajax({
            url: this.httpHost+'/seckill/accesspoint/api/v1/seckillapi/' + this.productid+"?userId="+this.userId+"&token="+this.token,
            type: "GET",
            dataType:'json',
            success: function(data) {
                if (data!=null && data.code == 200 && data.message == "success") {
                    self.seckillURL=data.payload.seckillApi;
                    if(successCall!=null)
                        successCall();
                }
                else if(failedCall!=null)
                    failedCall(data);
            },
            error: function(data) {
                if(failedCall!=null)
                    failedCall(data);
            }
        });
    },

    //获取商品信息
    //成功回调successCall(productInfo)    返回下发的url
    /*
     "payload": {
     "productInfo": {  # 商品信息
     "stockCount": "280", # 库存总量
     "startTime": "1489132029000", # 秒杀开始时间
     "endTime": "1489218435000",   # 秒杀结束时间
     "url": "/ec2daec0-0867-11e7-a746-69e281e33008/B879FE39D347EEF50175E57859830209" # 下发url
     },
     "currentTime": 1489474445134 # 当前时间
     }
     */
    //失败回调failedCall(json)
    productinfo: function(successCall,failedCall) {
        var self=this;
        return $.ajax({
                      url: this.httpHost+'/seckill/accesspoint/api/v1/products/' + this.productid+"?userId="+this.userId+"&token="+this.token,
                      type: "GET",
                      dataType:'json',
                      success: function(data) {
                            if (data!=null && data.code == 200 && data.message == "success") {
                                if(successCall!=null){
                                    self.currentMicSecond=data.payload.currentTime;
                                    self.serverStartMicSecond=data.payload.productInfo.startTime;
                                    self.serverEndMicSecond=data.payload.productInfo.endTime;
                                    self.seckillURL=data.payload.productInfo.url;
                                    self.timeEventUpdate();
                                    successCall(data.payload);
                                }
                            }
                            else if(failedCall!=null)
                                failedCall(data);
                      },
                      error: function(data) {
                            if(failedCall!=null)
                                failedCall(data);
                      }
        });
    },
    //排队时轮询秒杀状态
    //成功回调successCall(data)    true成功，false失败, null表示暂无记录
    //失败回调failedCall(json)
    
    apiStatus: function(successCall,failedCall) {
        var self=this;
        return $.ajax({
                      url: this.httpHost+'/seckill/accesspoint/api/v1/status/' + this.productid+"?userId="+this.userId+"&token="+this.token,
                      type: "GET",
                      dataType:'json',
                      success: function(data) {
                        if (data!=null) {
                            if(successCall!=null)
                                successCall(data);
                        }
                        else if(failedCall!=null)
                            failedCall(data);
                        },
                      error: function(data) {
                        if(failedCall!=null)
                            failedCall(data);
                        }
        });
    },

    //秒杀订单查询
    //成功回调successCall(request_id,payload)
    /*
     {
     "payload": {
     "productId": "B879FE39D347EEF50175E57859830204",
     "userId": "D2F75146B0C215E3C510B396FD920400s332",
     "secKillStatus": "00"   //秒杀状态 00 表示成功，
     "secKillTime" : "2017-03-14 17:25:33"
     },
     "message": "success",
     "code": 200,
     "request_id": "cd989906-40c5-44c4-a9cc-d7e410390e28"
     }
     */
    //失败回调failedCall(json)
    seckillQuery: function() {
        return $.ajax({
                      url: this.httpHost + this.seckillURL+"?userId="+this.userId+"&token="+this.token,
                      type: "GET",
                      dataType:'json',
                      success: function(data) {
                        if (data!=null && data.code == 200 && data.message == "success") {
                            if(successCall!=null)
                                successCall(data.request_id,data.payload);
                        }
                        else if(failedCall!=null)
                            failedCall(data);
                      },
                      error: function(data) {
                        if(failedCall!=null)
                            failedCall(data);
                        }
        });
    },
    
    //秒杀订单查询
    //成功回调successCall(request_id,payload)
    /*
     {
     "payload": [
     {
     "id": "587c2715-3166-4acf-9dea-ca1992fdb557",
     "productId": "ac2d2f83-b20d-4b49-aa61-f9071cdee8c8",
     "userId": "0091399",
     "seckillStatus": "00",
     "secKillTime": "2017-07-13T03:00:00.000Z",
     "isPaid": 0
     }
     ],
     "message": "success",
     "code": 200,
     "request_id": "7a4fd2b3-c87c-43ec-9b58-08aba55780b1"
     }
    */
    //失败回调failedCall(json)
    myOrders: function(successCall,failedCall) {
         return $.ajax({
                    url: this.httpHost + "/seckill/orderhandle/api/v1/"+this.userId+"/"+this.productid,
                    type: "GET",
                    dataType:'json',
                    success: function(data) {
                       if (data!=null && data.code == 200 && data.message == "success") {
                            if(successCall!=null)
                                successCall(data.request_id,data.payload);
                        }
                        else if(failedCall!=null)
                            failedCall(data);
                    },
                    error: function(data) {
                       if(failedCall!=null)
                            failedCall(data);
                       }
                    });
         }
})
