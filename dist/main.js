

function Main(){

}

$.extend(Main.prototype,{
    init:function(key){
        var self=this;
        this.nestJSSDK=sessionStorage.getItem("JS_SDK");
        this.bindEvents();
        if(key=='productiondetail' || key=='status' || key=='orderlist'){
            var product_id=self.getCookie('productId');
            if(product_id==null || product_id.length<1){
                product_id="?productId=";
                product_id=location.href.substr(location.href.indexOf(product_id)+product_id.length);
            }
            this.nestJSSDK=new NestCloudSeckill();
            this.nestJSSDK.init('http://47.94.129.86:8000',product_id,self.getCookie('userId'),self.getCookie('token'));
            if(key=='productiondetail'){
                this.nestJSSDK.updateTimeCallback=function(_nestJSSDK){
                    $('.buyNow').hide();
                    if(_nestJSSDK.seckStatus()==0){
                        $('.startTime_span').html('数据正在加载中');
                    }
                    else if(_nestJSSDK.seckStatus()==1){
                        var result=_nestJSSDK.seckStatusDate(_nestJSSDK.currentMicSecond,_nestJSSDK.serverStartMicSecond);
                        if(result.days>0){
                            result="离开始还剩："+result.days+"天"+result.hours+"时"+result.minutes+"分"+result.seconds+"秒";
                        }
                        else{
                            result="离开始还剩："+result.hours+"时"+result.minutes+"分"+result.seconds+"秒";
                        }
                        $('.startTime_span').html(result);
                    }
                    else if(_nestJSSDK.seckStatus()==2){
                        var result=_nestJSSDK.seckStatusDate(_nestJSSDK.currentMicSecond,_nestJSSDK.serverEndMicSecond);
                        if(result.days>0){
                            result="离结束还剩："+result.days+"天"+result.hours+"时"+result.minutes+"分"+result.seconds+"秒";
                        }
                        else{
                            result="离结束还剩："+result.hours+"时"+result.minutes+"分"+result.seconds+"秒";
                        }
                        $('.startTime_span').html(result);
                        $('.buyNow').show();
                    }
                    else if(self.nestJSSDK.seckStatus()==3){
                        $('.startTime_span').html('活动已结束');
                    }
                };

                this.nestJSSDK.productinfo(function(payload){
                    $('.ptxt').html("¥"+payload.productInfo.seckillPrice);
                    self.nestJSSDK.updateTimeCallback(self.nestJSSDK);
                },function(error){
                    $('.startTime_span').html('活动已结束');
                });
            }
            else if(key=='status'){

                self.showLoading();

                this.nestJSSDK.productinfo(function(payload){
                    self.statusChecking();

                },function(error){
                    self.hideLoading();
                    alert('加载数据出现错误');
                });


            }
            else if(key=='orderlist'){
                self.showLoading();
                self.nestJSSDK.myOrders(function(request_id,payload){
                        self.hideLoading();
                        var str='';
                        for(var i=0;i<payload.length;i++){
                            str+="<li>订单号："+payload[i].id+"</li>";
                        }
                        $('#lsOrder').html(str);
                },function(){
                        self.hideLoading();
                        alert('接口调用失败');
                });

            }

        }
    },

    statusChecking:function(){
        var self=this;
        //查询是否秒杀成功
        self.nestJSSDK.apiStatus(function(data){
            if(data.isSuccess=='success' && data.orderId!=null && data.orderId.length>0){
                self.hideLoading();
                $('.listBox').html("<p>用户的员工卡号："+sessionStorage.getItem("userId")+"</p><p>已成功抢购的订单号："+data.orderId+"</p>");
            }
            else if(data.isSuccess=='fail'){
                self.hideLoading();
                alert('对不起，本次秒杀抢购失败，请下次再来！');
            }
            else{
                setTimeout(function(){self.statusChecking();},3000);
            }
        },function(){
            setTimeout(function(){self.statusChecking();},3000);

        });
    },

    showLoading:function(){
        var loadHtml = '<div class="spinner_maskBox"><div class="spinner"><div class="spinner-container container1"><div class="circle1"></div><div class="circle2"></div><div class="circle3"></div><div class="circle4"></div></div><div class="spinner-container container2"><div class="circle1"></div><div class="circle2"></div><div class="circle3"></div><div class="circle4"></div></div><div class="spinner-container container3"><div class="circle1"></div><div class="circle2"></div><div class="circle3"></div><div class="circle4"></div></div></div></div>'
        $("body").append(loadHtml);
        $(".spinner").show();
    },
    hideLoading:function(){
        $(".spinner_maskBox").remove();
    },
    bindEvents:function(){
        var self = this
        $(document).on('click', '.loginBtn', function () {
            var username=$('#userName').val().trim();
            if(username.length<1){
                       alert("请输入用户名");
                       return;
            }
            self.showLoading();
            $.ajax({
                url: "http://killseck.shifu51.cn:8000/seckill-login",
                type: "POST",
                dataType:'json',
                data:{
                    "username":username,
                    "password":"123456"
                },
                success: function(data) {
                   self.hideLoading();
                   sessionStorage.setItem("userId",username);
                   sessionStorage.setItem("token",data.token);
                   location.href="prolist.html";
                },
                error: function(data) {
                   self.hideLoading();
                   alert("接口调用失败");
                }
            });
                       
            

        });
        $(document).on('click', '.buyBtn', function () {
            location.href="prodetail.html";
        });

        $(document).on('click','.buyNow',function(){
            self.showLoading();

            //秒杀开始，抢
            self.nestJSSDK.seckill(function(seckillStatus){
                if(seckillStatus=='queuing'){
                    self.hideLoading();
                    location.href="status.html";

                }
                else{
                    self.hideLoading();
                    alert('秒杀已经结束，谢谢参与。');
                }

            },function(data){
                alert(data);
            });

        });


    },
    getCookie:function(c_name){
        if (document.cookie.length>0){
            var c_start=document.cookie.indexOf(c_name + "=");
            if (c_start!=-1){
                var c_start=c_start + c_name.length+1;
                var c_end=document.cookie.indexOf(";",c_start);
                if (c_end==-1)
                    c_end=document.cookie.length;
                return unescape(document.cookie.substring(c_start,c_end));
            }
        }
        return "";
    }

});






var main=new Main();
